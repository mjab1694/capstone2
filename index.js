const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');

const app = express();
const port = process.env.PORT || 4000;

// MongoDB Connect
mongoose.connect('mongodb+srv://janeannmanguray:eE6yr8roSa5BmwpY@batch297.hvpd81d.mongodb.net/capstone2db?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => {
    console.log('Connected to MongoDB');
  })
  .catch((err) => {
    console.error('Error connecting to MongoDB', err);
  });

// Middleware
app.use(bodyParser.json());

// Routes
app.use('/api/Users', userRoutes);
app.use('/api/Products', productRoutes);
app.use('/api/Orders', orderRoutes);

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
