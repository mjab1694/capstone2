const mongoose = require('mongoose');

// Define the order item schema
const orderItemSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
});

// Define the order schema
const orderSchema = new mongoose.Schema({
  orderDate: {
    type: Date,
    required: true,
  },
  items: [orderItemSchema], // This creates an array of order items
  totalAmount: {
    type: Number,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

// Create and export the Order model
const Order = mongoose.model('Order', orderSchema);

module.exports = Order;
