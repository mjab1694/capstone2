const jwt = require('jsonwebtoken');
const secretKey = 'ecommerceAPI'; // Updated secret key


// Token creation
module.exports.createToken = (user) => {
  const data = {
    userId: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };

  return jwt.sign(data, secretKey, {});
};

// Token verification middleware
module.exports.verifyToken = (req, res, next) => {
  let token = req.headers.authorization;

  if (typeof token === 'undefined') {
    return res.status(403).json({ auth: 'Failed. No Token' });
  } else {
    token = token.slice(7); // Remove the "Bearer " prefix

    jwt.verify(token, secretKey, (err, decodedToken) => {
      if (err) {
        return res.status(401).json({ auth: 'Failed', message: err.message });
      } else {
        req.user = decodedToken;
        next();
      }
    });
  }
};

// Admin verification middleware
module.exports.verifyAdmin = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    return res.status(403).json({ auth: 'Failed', message: 'Action Forbidden' });
  }
};
