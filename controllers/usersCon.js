const User = require('../models/Users');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { createToken, verifyToken, verifyAdmin } = require('../auth');

// Create a new user
exports.createUser = async (req, res) => {
  try {
    const { username, email, password } = req.body;

    const hashedPassword = await bcrypt.hash(password, 10);

    const newUser = new User({ username, email, password: hashedPassword });
    const savedUser = await newUser.save();
    res.status(201).json(savedUser);
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
};

// Get all users (Admin only)
exports.getAllUsers = async (req, res) => {
  try {
    const user = verifyToken(req.headers.authorization);
    verifyAdmin(user);

    const users = await User.find();
    res.status(200).json(users);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// Get a user by ID (Admin only)
exports.getUserById = async (req, res) => {
  try {
    const user = verifyToken(req.headers.authorization);
    verifyAdmin(user);

    const requestedUserId = req.params.id;

    const userToReturn = await User.findById(requestedUserId);

    if (!userToReturn) {
      return res.status(404).json({ error: 'User not found' });
    }

    res.status(200).json(userToReturn);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// Update a user by ID (Admin only)
exports.updateUserById = async (req, res) => {
  try {
    const user = verifyToken(req.headers.authorization);
    verifyAdmin(user);

    const requestedUserId = req.params.id;
    const updatedUser = req.body;

    const result = await User.findByIdAndUpdate(requestedUserId, updatedUser, {
      new: true,
    });

    if (!result) {
      return res.status(404).json({ error: 'User not found' });
    }

    res.status(200).json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// Delete a user by ID (Admin only)
exports.deleteUserById = async (req, res) => {
  try {
    const user = verifyToken(req.headers.authorization);
    verifyAdmin(user);

    const requestedUserId = req.params.id;

    const result = await User.findByIdAndRemove(requestedUserId);

    if (!result) {
      return res.status(404).json({ error: 'User not found' });
    }

    res.status(204).end();
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// Login a user
exports.loginUser = async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = await User.findOne({ email });

    if (!user) {
      return res.status(401).json({ error: 'Invalid credentials' });
    }

    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!passwordMatch) {
      return res.status(401).json({ error: 'Invalid credentials' });
    }

    const token = createToken(user);
    res.status(200).json({ user, token });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};
