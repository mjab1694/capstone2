const Product = require('../models/Products');
const { createToken, verifyToken, verifyAdmin } = require('../auth');

// Create a new product (Admin only)
exports.createProduct = async (req, res) => {
  try {
    const newProduct = new Product(req.body);
    const savedProduct = await newProduct.save();
    res.status(201).json(savedProduct);
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
};

// Get all products (Admin only)
exports.getAllProducts = async (req, res) => {
  try {
    const products = await Product.find();
    res.status(200).json(products);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// Get all active products (All user)
exports.getAllActiveProducts = async (req, res) => {
  try {
    const activeProducts = await Product.find({ isActive: true });
    res.status(200).json(activeProducts);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// Get a product by ID (Admin only)
exports.getProductById = async (req, res) => {
  try {
    const product = await Product.findById(req.params.id);
    res.status(200).json(product);
  } catch (err) {
    res.status(404).json({ error: 'Product not found' });
  }
};

// Update a product by ID (Admin only)
exports.updateProductById = async (req, res) => {
  try {
    const updatedProduct = await Product.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res.status(200).json(updatedProduct);
  } catch (err) {
    res.status(404).json({ error: 'Product not found' });
  }
};

// Delete a product by ID (Admin only)
exports.deleteProductById = async (req, res) => {
  try {
    await Product.findByIdAndRemove(req.params.id);
    res.status(204).end();
  } catch (err) {
    res.status(404).json({ error: 'Product not found' });
  }
};
