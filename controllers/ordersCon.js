const Order = require('../models/Orders');
const { createToken, verifyToken, verifyAdmin } = require('../auth');


// Create a new order
exports.createOrder = async (req, res) => {
  try {
    const newOrder = new Order(req.body);
    const savedOrder = await newOrder.save();
    res.status(201).json(savedOrder);
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
};

// Get all orders
exports.getAllOrders = async (req, res) => {
  try {
    const orders = await Order.find();
    res.status(200).json(orders);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// Get an order by ID
exports.getOrderById = async (req, res) => {
  try {
    const order = await Order.findById(req.params.id);
    res.status(200).json(order);
  } catch (err) {
    res.status(404).json({ error: 'Order not found' });
  }
};

// Update an order by ID
exports.updateOrderById = async (req, res) => {
  try {
    const updatedOrder = await Order.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res.status(200).json(updatedOrder);
  } catch (err) {
    res.status(404).json({ error: 'Order not found' });
  }
};

// Delete an order by ID
exports.deleteOrderById = async (req, res) => {
  try {
    await Order.findByIdAndRemove(req.params.id);
    res.status(204).end();
  } catch (err) {
    res.status(404).json({ error: 'Order not found' });
  }
};
