const express = require('express');
const router = express.Router();
const userController = require('../controllers/usersCon');
const productController = require('../controllers/productsCon');
const orderController = require('../controllers/ordersCon');
const { verifyToken, verifyAdmin } = require('../auth');

// Create new product (Admin only)
router.post('/products', verifyToken, verifyAdmin, productController.createProduct);

// Get all product (Admin only)
router.get('/products', verifyToken, verifyAdmin, productController.getAllProducts);

// Get all product (All user)
router.get('/products/active', productController.getAllActiveProducts);

// Get product by Id (Admin only)
router.get('/products/:id', verifyToken, verifyAdmin, productController.getProductById);

// Update product (Admin only)
router.put('/products/:id', verifyToken, verifyAdmin, productController.updateProductById);

// Delete product (Admin only)
router.delete('/products/:id', verifyToken, verifyAdmin, productController.deleteProductById);

module.exports = router;
