const express = require('express');
const router = express.Router();
const userController = require('../controllers/usersCon');
const productController = require('../controllers/productsCon');
const orderController = require('../controllers/ordersCon');
const { verifyToken, verifyAdmin } = require('../auth');


// Create order
router.post('/createOrder', orderController.createOrder);

// Get all orders (Admin only)
router.get('/orders', verifyToken, verifyAdmin, orderController.getAllOrders);

// Get an order by ID (Admin only)
router.get('/orders/:id', verifyToken, verifyAdmin, orderController.getOrderById);

// Update an order by ID (Admin only)
router.put('/orders/:id', verifyToken, verifyAdmin, orderController.updateOrderById);

// Delete an order by ID (Admin only)
router.delete('/orders/:id', verifyToken, verifyAdmin, orderController.deleteOrderById);

module.exports = router;
