const express = require('express');
const router = express.Router();
const userController = require('../controllers/usersCon');
const productController = require('../controllers/productsCon');
const orderController = require('../controllers/ordersCon');
const { createUser, getAllUsers, getUserById, updateUserById, deleteUserById, loginUser, } = require('../controllers/usersCon');

// Create a new user
router.post('/createUser', createUser);

// Get all users (Admin only)
router.get('/getAllUsers', getAllUsers);

// Get a user by ID (Admin only)
router.get('/getUserById/:id', getUserById);

// Update a user by ID (Admin only)
router.put('/updateUserById/:id', updateUserById);

// Delete a user by ID (Admin only)
router.delete('/deleteUserById/:id', deleteUserById);

// Login a user
router.post('/loginUser', loginUser);

module.exports = router;
